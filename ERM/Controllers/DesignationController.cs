﻿using Microsoft.AspNetCore.Mvc;
using ERM.Core.Models;
using ERM.Service.Services.Interface;
using System.Net;
using System.Web.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;


namespace ERM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DesignationController : ControllerBase
    {
        public IDesignationService _service;
        public DesignationController(IDesignationService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetDesignationList()
        {
            var designationDetailsList = await _service.GetAllDesignations();
            if (designationDetailsList == null)
            {
                return NotFound();
            }
            return Ok(designationDetailsList);
        }
        [HttpGet("{designationId}")]
        public async Task<IActionResult> GetDesignationById(int designationId)
        {
            var designationDetailsList = await _service.GetDesignationById(designationId);

            if (designationDetailsList != null)
            {
                return Ok(designationDetailsList);
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public async Task<IActionResult> CreateDesignation(Designation desc)
        {
            try 
            {
               var result=await _service.CreateDesignation(desc);
                if (result==true)
                    return Ok(result);
                else
                    return Content("Something went wrong");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
           
        }
        [HttpPut]
        public async Task<IActionResult> UpdateDesignation(Designation desc)
        {
            try
            {
                var result = await _service.UpdateDesignation(desc);
                if (result==true)
                    return Ok(result);
                else
                    return Content("Something went wrong");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpDelete("{designationId}")]
        public async Task<IActionResult> DeleteDesignation(int designationId)
        {
            try
            {
                var result = await _service.DeleteDesignaton(designationId);
                if (result==true)
                    return Ok(result);
                else
                    return Content("Something went wrong");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
