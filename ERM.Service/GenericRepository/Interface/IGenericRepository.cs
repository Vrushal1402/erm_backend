﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.GenericRepository.Interface
{
    public interface IGenericRepository<T> where T : class
    {
        // IEnumerable<T> getAll();
        Task<IEnumerable<T>> getAll();
        Task<T> getById(int id);
        Task Insert(T entity);
        void Update(T entity);
        void Delete(T entity);


    }
}
