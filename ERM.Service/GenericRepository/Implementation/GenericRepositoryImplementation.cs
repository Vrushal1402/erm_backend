﻿//using Microsoft.EntityFrameworkCore;
//using ERM.Core.DbContext;
using ERM.Core.DbContext;
using ERM.Service.GenericRepository.Interface;
using Microsoft.EntityFrameworkCore;


//using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.GenericRepository.Implementation
{
    public class GenericRepositoryImplementation<T> : IGenericRepository<T> where T : class
    {
        private readonly ApplicationContext _context;
        private readonly System.Data.Entity.DbSet<T> entities;
        public GenericRepositoryImplementation(ApplicationContext context)
        {
            _context = context;
            //this.entities = context.Set<T>();
        }



        public async void Delete(T entity)
        {
            //if (entity == null)
            //{
            //    throw new ArgumentNullException("entity");
            //}
            // _context.Set<T>().Remove(entity);
            //var result = _context.SaveChanges();
            //if (result>0)
            //    return true;
            //else
            //    return false;
            _context.Set<T>().Remove(entity);
        }

        public async Task<IEnumerable<T>> getAll()
        {
            // return entities.ToList();
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> getById(int id)
        {
            //return entities.Find(id);
            return await _context.Set<T>().FindAsync(id);
        }

        //public async Task<bool> Insert(T entity)
        //{
        //    if (entity == null)
        //    {
        //        throw new ArgumentNullException("entity");
        //    }
        //    entities.Add(entity);
        //   var result= _context.SaveChanges();
        //    if (result>0)
        //        return true;
        //    else
        //        return false;
        //}
        public async Task Insert(T entity)
        {
            //if (entity == null)
            //{
            //    throw new ArgumentNullException("entity");
            //}
            //entities.Add(entity);
            //var result = _context.SaveChanges();
            //if (result>0)
            //    return true;
            //else
            //    return false;
            await _context.Set<T>().AddAsync(entity);
        }

        public async void Update(T entity)
        {
            // if (entity == null)
            // {
            //     throw new ArgumentNullException("entity");
            // }
            // entities.Attach(entity);
            // _context.Entry(entity).State = (Microsoft.EntityFrameworkCore.EntityState)System.Data.Entity.EntityState.Modified;
            //var result= _context.SaveChanges(true);
            // if (result>0)
            //     return true;
            // else
            //     return false;
            _context.Set<T>().Update(entity);
        }
    }
}
