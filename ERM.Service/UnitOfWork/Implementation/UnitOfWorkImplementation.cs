﻿using ERM.Core.DbContext;
using ERM.Service.Repository.Implementation;
using ERM.Service.Repository.Interface;
using ERM.Service.UnitOfWork.Interface;
//using ERM.Service.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.UnitOfWork.Implementation
{
    public class UnitOfWorkImplementation : IUnitOfWork
    {
        private IRepository _repository;
        private IDesignationRepository _designationRepository;
        private readonly string _connectionString;
        private ApplicationContext _dbContext;

        public UnitOfWorkImplementation(ApplicationContext context)
        {
            _dbContext = context;
        }

        public IRepository Repository
        {
            get { return _repository = _repository ?? new RepositoryImplementation(_dbContext); }
        }
        public IDesignationRepository designationRepository
        {
            get { return (IDesignationRepository)(_designationRepository = _designationRepository ?? new DesignationImplementantation(_dbContext)); }
        }
       // IRepository IUnitOfWork.Repository => throw new NotImplementedException();

        //IDesignationRepository IUnitOfWork.designationRepository => throw new NotImplementedException();
        void IUnitOfWork.Commit()
        {
            throw new NotImplementedException();
        }

        void IUnitOfWork.Rollback()
        {
            throw new NotImplementedException();
        }

        Task IUnitOfWork.CommitAsync()
        {
            throw new NotImplementedException();
        }

        Task IUnitOfWork.RollbackAsync()
        {
            throw new NotImplementedException();
        }
        int IUnitOfWork.Save()
        {
            return _dbContext.SaveChanges();
        }
    }
}
