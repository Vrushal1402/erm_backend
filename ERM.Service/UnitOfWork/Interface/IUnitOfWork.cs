﻿using ERM.Service.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.UnitOfWork.Interface
{
    public interface IUnitOfWork
    {
        public  IRepository Repository { get; }
        public IDesignationRepository designationRepository { get; }
        void Commit();
        void Rollback();
        Task CommitAsync();
        Task RollbackAsync();
        int Save();

    }
}
