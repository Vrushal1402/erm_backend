﻿using ERM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.Services.Interface
{
    public interface IDesignationService
    {
        Task<bool> CreateDesignation(Designation designation);
        Task<IEnumerable<Designation>> GetAllDesignations();
        Task<Designation> GetDesignationById(int designationId);
        Task<bool> UpdateDesignation(Designation designationDetails);
        Task<bool> DeleteDesignaton(int designationId);
    }
}
