﻿using ERM.Service.Services.Interface;
using ERM.Service.UnitOfWork.Implementation;
using ERM.Service.UnitOfWork.Interface;
//using ERM.Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.Services.Implementation
{
    public class ServiceImplementation : IService
    {

        
        public ServiceImplementation()
        {
          
        }

        public void Create(string connectionString, string tableName, InsertRequest insertRequest)
        {
            throw new NotImplementedException();
        }

        public void Delete(string connectionString, string tableName, DeleteRequest deleteRequest)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetAll()
        {
            throw new NotImplementedException();
        }

        public object GetById(string connectionString, string tableName, int id)
        {
            throw new NotImplementedException();
        }

        public void Update(string connectionString, string tableName, UpdateRequest updateRequest)
        {
            throw new NotImplementedException();
        }
    }
}
