﻿using ERM.Core.Models;
using ERM.Service.Services.Interface;
using ERM.Service.UnitOfWork.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.Services.Implementation
{
    public class DesignationImplementation : IDesignationService
    {
        private readonly IUnitOfWork _unitOfWork;
        public DesignationImplementation(IUnitOfWork unitOfWork)
        {
            _unitOfWork= unitOfWork;
        }

        public async Task<bool> CreateDesignation(Designation designation)
        {
            //await _unitOfWork.designationRepository.CreateDesignation(designation);
            designation.Id=0;
            await _unitOfWork.designationRepository.Insert(designation);
            var result = _unitOfWork.Save();
            if (result > 0)
                return true;
            else
                return false;
        }

        public async Task<bool> DeleteDesignaton(int designationId)
        {
            if (designationId > 0)
            {
                var designationDetails = await _unitOfWork.designationRepository.getById(designationId);
                if (designationDetails != null)
                {
                     _unitOfWork.designationRepository.Delete(designationDetails);
                    var result = _unitOfWork.Save();

                    if (result > 0)
                        return true;
                    else
                        return false;
                }
            }
            return false;
        }

        public async Task<IEnumerable<Designation>> GetAllDesignations()
        {
            var designationDetailsList =  await _unitOfWork.designationRepository.getAll();
            return designationDetailsList;
        }

        public async Task<Designation> GetDesignationById(int designationId)
        {
            if (designationId > 0)
            {
                var designationDetails = await _unitOfWork.designationRepository.getById(designationId);
                if (designationDetails != null)
                {
                    return designationDetails;
                }
            }
            return null;
        }

        public async Task<bool> UpdateDesignation(Designation designationDetails)
        {
            if (designationDetails != null)
            {
                var designation = await _unitOfWork. designationRepository.getById(designationDetails.Id);
                if (designation != null)
                {
                    designation.Name= designationDetails.Name;
                    designation.IsActive= designationDetails.IsActive;
                     _unitOfWork.designationRepository.Update(designation);
                    var result = _unitOfWork.Save();

                    if (result > 0)
                        return true;
                    else
                        return false;
                }
            }
            return false;
        }
    }
}
