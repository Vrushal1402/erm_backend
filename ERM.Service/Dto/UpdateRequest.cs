﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service
{
    public class UpdateRequest
    {
        public string[] Columns { get; set; }
        public List<object> ColumnsValues { get; set; } = new List<object>();
        public string primaryKeyColumn { get; set; }
       public  int primaryKeyValue {  get; set; }
    }
}
