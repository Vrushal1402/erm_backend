﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service
{
    public class DeleteRequest
    {
        public string primaryKeyColumn { get; set; }
        public int primaryKeyValue { get; set; }
    }
}
