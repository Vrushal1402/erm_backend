﻿//using ERM.Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.Repository.Interface
{
    public interface IRepository
    {

        IEnumerable<object> GetAll(string connectionString, string tableName);

        object GetById(string connectionString, string tableName, int id);

        void Create(string connectionString, string tableName, InsertRequest insertRequest);

        void Update(string connectionString, string tableName, UpdateRequest updateRequest);

        void Delete(string connectionString, string tableName, DeleteRequest deleteRequest);
    }
}
