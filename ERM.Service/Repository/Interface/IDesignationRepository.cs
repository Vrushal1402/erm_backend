﻿using ERM.Core.Models;
using ERM.Service.GenericRepository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.Repository.Interface
{
    public interface IDesignationRepository:IGenericRepository<Designation>
    {
      

    }
}
