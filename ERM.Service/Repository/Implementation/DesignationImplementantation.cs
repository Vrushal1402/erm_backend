﻿using ERM.Core.DbContext;
using ERM.Core.Models;
using ERM.Service.GenericRepository.Implementation;
using ERM.Service.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERM.Service.Repository.Implementation
{
    public class DesignationImplementantation : GenericRepositoryImplementation<Designation>, IDesignationRepository
    {
        private readonly ApplicationContext _dbContext;
        public DesignationImplementantation(ApplicationContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

       
    }
}
